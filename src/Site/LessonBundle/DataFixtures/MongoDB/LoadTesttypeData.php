<?php 
namespace Site\LessonBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\LessonBundle\Document\Testtype;

class loadTesttypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $testtypes = array(
            array(
                'name' => 'type1',
                'teacher' => 'lichnow'
            ),
            array(
                'name' => 'type2',
                'teacher' => 'lichnow'
            ),
            array(
                'name' => 'type3',
                'teacher' => 'lichnow'
            ),
        );
        $this->insertData($manager,$testtypes);
    }

    protected function insertData($manager,$testtypes)
    {
        foreach ($testtypes as $key => $value) {
            $testtype = new Testtype();
            $testtype->setName($value['name']);
            $testtype->setTeacher($this->getReference($value['teacher'].'teacher'));
            $manager->persist($testtype);
            $manager->flush();
            $this->addReference($value['name'].'type',$testtype);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }
}
 ?>