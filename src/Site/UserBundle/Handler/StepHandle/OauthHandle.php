<?php

namespace Site\UserBundle\Handler\StepHandle;

use Site\UserBundle\Form\Type\OauthCreateFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Site\CoreBundle\InterFacer\Foundation as BaseClass;

class OauthHandle extends BaseClass {
    public function handle() {
        $sessionuser = $this->getUser();
        if ($sessionuser) {
            if ($sessionuser->getEmail() == '' || $sessionuser->getPassword() == '') {
                $create_result = $this->createHandle($sessionuser);
                $bind_result = $this->bindHandle($sessionuser);
                if($create_result === true || $bind_result === true)
                {
                    return true;
                }
                return $this->render('SiteUserBundle:Oauth:index.html.twig',array(
                    'create' => $create_result,
                    'bind' => $bind_result
                ));
            }
            return true;
        }
        return $this->redirect($this->generateUrl('fos_user_security_login'));
    }
    private function bindFormView($sessionuser)
    {
        $getter_name = 'get' . $sessionuser->getOauthType() . 'Name';
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername($sessionuser->$getter_name());
        $form = $this->createNamedFormBuilder('bindform',$user,array('validation_groups' => array('Security')))
            ->add('username', null, array(
                'label' => '用户名:', 
                'attr' => array(
                    'placeholder' => '请输入用户名/邮箱地址'
                )
            ))->add('plainPassword', 'password', array(
                'label' => '密码:',
                'attr' => array(
                    'placeholder' => '请输入密码'
                )
            ))->add('bind', 'submit',array(
                'label' => '绑定账户'
            ))->getForm();
        return $form;
    }
    private function bindHandle($sessionuser)
    {
        $request = $this->getRequest();
        $form = $this->bindFormView($sessionuser);
        $errors = '';
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $request->request->has('bindform')) {
            $newuser = $form->getData();
            $checkpassword = $this->getLibrary()->get('SiteUserBundle:User')
            ->checkPassword($newuser->getUsername(),$newuser->getplainPassword());
            if ($form->isValid() && $checkpassword) {
                return $this->bindUser($sessionuser,$newuser);
            }elseif ($form->isValid() && !$checkpassword) {
                $errors = array(array('message' => '用户名或密码错误'));
            }else{
                $errors = $this->get('validator')->validate($form->getData(), array('Security'));
            }
        }
        return array(
            'form' => $form->createView(),
            'errors' => $errors
        );
    }
    private function bindUser($olduser,$newuser)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('username' => $newuser->getUsername()));
        $setter = 'set' . $olduser->getOauthType();
        $getter = 'get' . $olduser->getOauthType();
        $setter_id = $setter . 'Id';
        $setter_name = $setter . 'Name';
        $setter_token = $setter . 'AccessToken';
        $getter_id = $getter . 'Id';
        $getter_name = $getter . 'Name';
        $getter_token = $getter . 'AccessToken';
        $user->$setter_id($olduser->$getter_id());
        $user->$setter_name($olduser->$getter_name());
        $user->$setter_token($olduser->$getter_token());
        $userimg = $this->getLibrary()->get('SiteUserBundle:User')->createOauthimg($user,$olduser);
        if($userimg)
            $user->setUserimg($userimg);
        $userManager->updateUser($user,false);
        $this->get('doctrine_mongodb')->getManager()->flush();
        $firewall = $this->container->getParameter('fos_user.firewall_name');
        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
        $this->get('security.context')->setToken($token);
        return true;
    }
    private function createFormView($sessionuser) {
        $request = $this->getRequest();
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername($sessionuser->getUsername());
        $user->setEmail($sessionuser->getEmail());
        $form = $this->createNamedForm('createform',new OauthCreateFormType(), $user);
        $form->setData($user);
        return $form;
    }
    private function createHandle($sessionuser)
    {
        $request = $this->getRequest();
        $form = $this->createFormView($sessionuser);
        $errors = '';
        $form->handleRequest($request);
        if ($request->isMethod('POST') && $request->request->has('createform')) {
            if ($form->isValid()) {
                return $this->createUser($sessionuser,$form->getData());
            } 
            $errors = $this->get('validator')->validate($form->getData(), array('SiteRegistration', 'Registration'));
        }
        return array(
            'form' => $form->createView(),
            'errors' => $errors
        );
    }
    private function createUser($olduser,$newuser) {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername($newuser->getUsername());
        $user->setEmail($newuser->getEmail());
        $user->setPlainPassword($newuser->getplainPassword());
        $setter = 'set' . $olduser->getOauthType();
        $getter = 'get' . $olduser->getOauthType();
        $setter_id = $setter . 'Id';
        $setter_name = $setter . 'Name';
        $setter_token = $setter . 'AccessToken';
        $getter_id = $getter . 'Id';
        $getter_name = $getter . 'Name';
        $getter_token = $getter . 'AccessToken';
        $user->setEnabled($olduser->getEnabled());
        $user->$setter_id($olduser->$getter_id());
        $user->$setter_name($olduser->$getter_name());
        $user->$setter_token($olduser->$getter_token());
        $userimg = $this->getLibrary()->get('SiteUserBundle:User')->createOauthimg($user,$olduser);
        if($userimg)
            $user->setUserimg($userimg);
        $userManager->updateUser($user);
        $firewall = $this->container->getParameter('fos_user.firewall_name');
        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
        $this->get('security.context')->setToken($token);
        return true;
    }
}