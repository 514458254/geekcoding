<?php 
namespace Site\LessonBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\LessonBundle\Document\Video;

class LoadVideoData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $videos = array(
            array(
                'playtimes' => 0,
                'downpath' => 'uploads/videos/home/homevideo.mov',
                'imgpath' => 'uploads/videos/home/homeimg.jpg',
                'public' => true,
                'ctime' => new \DateTime(),
                'chapter' => 'symfony2first',
            ),
            array(
                'playtimes' => 0,
                'downpath' => 'uploads/videos/home/homevideo.mov',
                'imgpath' => 'uploads/videos/home/homeimg.jpg',
                'public' => true,
                'ctime' => new \DateTime(),
                'chapter' => 'symfony2second',
            ),
            array(
                'playtimes' => 0,
                'downpath' => 'uploads/videos/home/homevideo.mov',
                'imgpath' => 'uploads/videos/home/homeimg.jpg',
                'public' => true,
                'ctime' => new \DateTime(),
                'chapter' => 'symfony2third',
            ),
        );
        $this->insertData($manager,$videos);
    }

    protected function insertData($manager,$videos)
    {
        foreach ($videos as $key => $value) {
            $video = new Video();
            $video->setPlaytimes($value['playtimes']);
            $video->setDownpath($value['downpath']);
            $video->setImgpath($value['imgpath']);
            $video->setCtime($value['ctime']);
            $video->setPublic($value['public']);
            $video->setChapter($this->getReference($value['chapter'].'chapter'));
            $manager->persist($video);
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }
}
 ?>