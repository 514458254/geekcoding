<?php
 
namespace Site\CoreBundle\InterFacer;
 
use Symfony\Component\HttpFoundation\Request;

interface InitControllerInterface
{
	public function init();
    public function createFormBuilder($data = null, array $options = array());
	public function createNamedForm($name, $type = 'form', $data = null, array $options = array());
    public function createNamedFormBuilder($name, $data = null, array $options = array());
}