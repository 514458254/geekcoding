<?php 
namespace Site\LessonBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
/**
 * @MongoDB\Document(collection="lesson_videos",repositoryClass="Site\LessonBundle\Repository\VideoRepository")
 */
class Video
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Chapter",inversedBy="video") */
    private $chapter;

    /**
    * @MongoDB\Int
    */
    protected $playtimes;

    /**
    * @MongoDB\String
    */
    protected $downpath;

    /**
    * @MongoDB\String
    */
    protected $imgpath;

    /**
     * @MongoDB\Boolean
     */
    protected $public;

    /**
    * @MongoDB\Date
    */
    protected $ctime;

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chapter
     *
     * @param Site\LessonBundle\Document\Chapter $chapter
     * @return self
     */
    public function setChapter(\Site\LessonBundle\Document\Chapter $chapter)
    {
        $this->chapter = $chapter;
        return $this;
    }

    /**
     * Get chapter
     *
     * @return Site\LessonBundle\Document\Chapter $chapter
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Set playtimes
     *
     * @param int $playtimes
     * @return self
     */
    public function setPlaytimes($playtimes)
    {
        $this->playtimes = $playtimes;
        return $this;
    }

    /**
     * Get playtimes
     *
     * @return int $playtimes
     */
    public function getPlaytimes()
    {
        return $this->playtimes;
    }

    /**
     * Set downpath
     *
     * @param string $downpath
     * @return self
     */
    public function setDownpath($downpath)
    {
        $this->downpath = $downpath;
        return $this;
    }

    /**
     * Get downpath
     *
     * @return string $downpath
     */
    public function getDownpath()
    {
        return $this->downpath;
    }

    /**
     * Set imgpath
     *
     * @param string $imgpath
     * @return self
     */
    public function setImgpath($imgpath)
    {
        $this->imgpath = $imgpath;
        return $this;
    }

    /**
     * Get imgpath
     *
     * @return string $imgpath
     */
    public function getImgpath()
    {
        return $this->imgpath;
    }

    /**
     * Set public
     *
     * @param boolean $public
     * @return self
     */
    public function setPublic($public)
    {
        $this->public = $public;
        return $this;
    }

    /**
     * Get public
     *
     * @return boolean $public
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set ctime
     *
     * @param date $ctime
     * @return self
     */
    public function setCtime($ctime)
    {
        $this->ctime = $ctime;
        return $this;
    }

    /**
     * Get ctime
     *
     * @return date $ctime
     */
    public function getCtime()
    {
        return $this->ctime;
    }
}
