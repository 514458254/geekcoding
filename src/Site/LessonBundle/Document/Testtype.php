<?php 
namespace Site\LessonBundle\Document;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Type as JType;
use JMS\Serializer\Annotation\Exclude as JExclude;
use JMS\Serializer\Annotation\ExclusionPolicy as JExclusionPolicy;
use JMS\Serializer\Annotation\Expose as JExpose;
use JMS\Serializer\Annotation\Groups as JGroups;
use JMS\Serializer\Annotation\Accessor as JAccessor;
use JMS\Serializer\Annotation\VirtualProperty as JVirtualProperty;
use JMS\Serializer\Annotation\SerializedName as JSerializedName;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="testtypes",repositoryClass="Site\LessonBundle\Repository\TesttypeRepository")
 */
class Testtype
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;


    /** 
     * @MongoDB\ReferenceMany(targetDocument="Test", mappedBy="testtype")
     * @JType("ArrayCollection<Site\LessonBundle\Document\Test>")
     * @JAccessor(getter="getTests")
     * @JExpose
     */
    private $tests;

    /**
     * @MongoDB\String  @MongoDB\UniqueIndex
     */
    protected $name;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Site\TeacherBundle\Document\Teacher",inversedBy="testtypes")
     * @JMS\MaxDepth(1)
     */
    private $teacher;

    public function __construct()
    {
        $this->tests = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add test
     *
     * @param Site\LessonBundle\Document\Test $test
     */
    public function addTest(\Site\LessonBundle\Document\Test $test)
    {
        $this->tests[] = $test;
    }

    /**
     * Remove test
     *
     * @param Site\LessonBundle\Document\Test $test
     */
    public function removeTest(\Site\LessonBundle\Document\Test $test)
    {
        $this->tests->removeElement($test);
    }

    /**
     * Get tests
     *
     * @return Doctrine\Common\Collections\Collection $tests
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set teacher
     *
     * @param Site\TeacherBundle\Document\Teacher $teacher
     * @return self
     */
    public function setTeacher(\Site\TeacherBundle\Document\Teacher $teacher)
    {
        $this->teacher = $teacher;
        return $this;
    }

    /**
     * Get teacher
     *
     * @return Site\TeacherBundle\Document\Teacher $teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /** 
     * @JVirtualProperty
     * @JSerializedName("foo")
     */
    public function bar(){
        return $this->tests;
    }
}
