define([
    './globalRoute',
    '../controllers/lessonCtrl',
    '../services/lessonService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
            .state('teacher_home.lesson', {
                url: "^"+Routing.generate('site_teacher_template_lesson'),
                views: {
                    "teacher-content": {
                        templateUrl: Routing.generate('site_teacher_template_lesson'),
                        controller: 'lessonCtrl',
                        resolve: {
                            lessoninfo: function (viewService) {
                                return viewService.getData('lessonService');
                            }
                        }
                    }
                }
            });
    });
});