define([
    './globalRoute',
    '../controllers/planCtrl',
    '../services/planService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
            .state('teacher_home.plan', {
                url: "^"+Routing.generate('site_teacher_template_plan'),
                views: {
                    "content": {
                        templateUrl: Routing.generate('site_teacher_template_plan'),
                        controller: 'planCtrl',
                        resolve: {
                            planinfo: function (viewService) {
                                return viewService.getData('planService');
                            }
                        }
                    }
                }
            });
    });
});