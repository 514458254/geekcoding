<?php

namespace Site\HomeBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Site\HomeBundle\Controller\BaseController as BaseController;

class PublicController extends BaseController
{
	/**
	 * @Template
	 */
	public function headerAction()
	{
		return array('navigates' => $this->NavigateModel->getList());
	}

	/**
	 * @Template
	 */
	public function footerAction(){}

	/**
	 * @Template
	 */
	public function localAction(Array $local){
		return array('local' => $local);
	}
}