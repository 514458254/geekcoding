<?php 
namespace Site\LessonBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\LessonBundle\Document\Test;

class LoadTestData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $tests = array(
            array(
                'name' => '测试1',
                'type' => 'type1'
            ),
            array(
                'name' => '测试2',
                'type' => 'type2'
            ),
            array(
                'name' => '测试3',
                'type' => 'type3'
            ),
        );
        $this->insertData($manager,$tests);
    }

    protected function insertData($manager,$tests)
    {
        foreach ($tests as $key => $value) {
            $test = new Test();
            $test->setName($value['name']);
            $test->setType($this->getReference($value['type'].'type'));
            $manager->persist($test);
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 12;
    }
}
 ?>