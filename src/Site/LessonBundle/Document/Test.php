<?php 
namespace Site\LessonBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="tests",repositoryClass="Site\LessonBundle\Repository\TestRepository")
 */
class Test
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** @MongoDB\ReferenceOne(targetDocument="Testtype",inversedBy="tests") */
    private $type;

    /**
     * @MongoDB\String  @MongoDB\UniqueIndex
     */
    protected $name;

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param Site\LessonBundle\Document\Testtype $type
     * @return self
     */
    public function setType(\Site\LessonBundle\Document\Testtype $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return Site\LessonBundle\Document\Testtype $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }
}
