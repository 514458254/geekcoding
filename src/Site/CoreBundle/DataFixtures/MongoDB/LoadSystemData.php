<?php 
namespace Site\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\CoreBundle\Document\System;

class LoadSystemData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $system = new System();
        $system->setTitle('极客编码');
        $system->setMeta('PHP视频,Ruby视频,Rails视频');
        $system->setDescription('极客编码是国内最领先的编程实战技术在线教育平台');
        $system->setShow(true);
        $manager->persist($system);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
 ?>