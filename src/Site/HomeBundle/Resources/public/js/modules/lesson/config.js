define(['angular','../core/loader','../global/bootstrap', 'angularresource','angularui'], function (angular,loader) {
    var config = angular.module('lesson', ['ngResource','ui.router','global'])
        .config(['$routeProvider', '$interpolateProvider', '$locationProvider',function ($routeProvider, $interpolateProvider, $locationProvider) {
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
            // $locationProvider.html5Mode(true);
        }]).run(["$rootScope", "$location","$injector","$state",function($rootScope, $location,$injector,$state){
        	loaderService = $injector.get("loaderService");
            $rootScope.init = function(){
                loader.pageloader.show(".lesson-container-loader",'window','window','circle');
            }
            $rootScope.$on('$viewContentLoaded', function(){
                loader.pageloader.hide('circle');
                if($state.current.name == 'lesson' && !loader.pageloader.isShow()){
                    loader.pageloader.show(".lesson-loader",'parent');
                }
            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState){
            });
        }]);
        return config;
});
