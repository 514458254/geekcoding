define([
    './globalRoute',
    '../controllers/centerCtrl',
    '../services/centerService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
            .state('user_home.center', {
                url: "^"+Routing.generate('site_user_template_center'),
                views: {
                    "user-content": {
                        templateUrl: Routing.generate('site_user_template_center'),
                        controller: 'centerCtrl',
                        resolve: {
                            centerinfo: function (viewService) {
                                return viewService.getData('centerService');
                            }
                        }
                    }
                }
            });
    });
});