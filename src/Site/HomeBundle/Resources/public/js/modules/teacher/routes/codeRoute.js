define([
    './globalRoute',
    '../controllers/codeCtrl',
    '../services/codeService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
        .state('teacher_home.code', {
                url: "^"+Routing.generate('site_teacher_template_code'),
                views: {
                    "teacher-content": {
                        templateUrl: Routing.generate('site_teacher_template_code'),
                        controller: 'codeCtrl',
                        resolve: {
                            codeinfo: function (viewService) {
                                return viewService.getData('codeService');
                            }
                        }
                    }
                }
        });
    });
});