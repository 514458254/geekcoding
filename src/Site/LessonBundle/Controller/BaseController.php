<?php

namespace Site\LessonBundle\Controller;
use Site\CoreBundle\InterFacer\AnnotationController;
class BaseController extends AnnotationController{
	public $LessonVideoModel;
	public $LessonTypeModel;
	public $LessonModel;
	public function initialize()
	{
		$this->LessonVideoModel = $this->getModel()->get('SiteLessonBundle:Video');
		$this->LessonTypeModel = $this->getModel()->get('SiteLessonBundle:Type');
		$this->LessonModel = $this->getModel()->get('SiteLessonBundle:Lesson');
		$this->UserLib = $this->getLibrary()->get('SiteUserBundle:User');
	}
}