<?php

namespace Site\CoreBundle\Library\File;
use Site\CoreBundle\InterFacer\Library;
class Oss extends Library
{
	public function get($ossname)
	{
		$classname = 'Site\\CoreBundle\\Library\\File\\Oss\\'.ucfirst($ossname);
		return new $classname($this->container);
	}
}