define(['jquery'], function ($) {
	var $win = $(window), //窗口对象
	    $body = $('body'), //文档对象
	    $bodyTop = parseInt($body.css('margin-top'), 10), //文档顶部距离
	    $navHeight = $('.navbar').first().height(), //主导航栏高度
	    $subnav = $('.subnav'),
	    $subnavHeight = $('.subnav').first().height(), //子导航栏高度
	    $subnavTop = $('.subnav').length && $('.subnav').offset().top - $navHeight, //子导航与主导航距离
	    $subnavFixed = 0; //自导航是否固定
	$subNavScroll = function() {
        var i, scrollTop = $win.scrollTop();
        if (scrollTop >= $subnavTop && !$subnavFixed) {
            $subnavFixed = 1;
            $subnav.addClass('subnav-fixed');
            $body.css('margin-top', $bodyTop + $subnavHeight + parseInt($subnav.css('margin-bottom')) + 'px');
        } else if (scrollTop <= $subnavTop && $subnavFixed) {
            $subnavFixed = 0;
            $subnav.removeClass('subnav-fixed');
            $body.css('margin-top', $bodyTop + 'px');
        }
    };
    $(function(){
    	$subNavScroll.call();
        $(document).on('scroll', $win, $subNavScroll);
    });
});