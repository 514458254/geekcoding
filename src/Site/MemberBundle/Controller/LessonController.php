<?php
namespace Site\MemberBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * @Route("/lesson",service="site_member.lesson")
 */
class LessonController extends BaseController
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
    	$data['lessons'] = $this->getUser()->getLessons();
        return $this->render('SiteMemberBundle:Lesson:index.html.twig',$data);
    }
}