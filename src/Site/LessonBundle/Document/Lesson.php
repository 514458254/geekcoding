<?php 
namespace Site\LessonBundle\Document;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="lessons",repositoryClass="Site\LessonBundle\Repository\LessonRepository")
 * @JMS\ExclusionPolicy("none")
 */
class Lesson
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Type",inversedBy="lessons")
     */
    private $type;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Level",inversedBy="lessons")
     * @JMS\Groups({"getlesson"}) 
     * @JMS\Expose
     */
    private $level;

    /** 
     * @MongoDB\ReferenceOne(targetDocument="Site\TeacherBundle\Document\Teacher",inversedBy="lessons")
     */
    private $teacher;

    /** 
     * @MongoDB\ReferenceMany(targetDocument="Site\UserBundle\Document\User",inversedBy="lessons")
     */
    private $users;

    /** 
     * @MongoDB\ReferenceMany(targetDocument="Category", mappedBy="lesson")
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    private $categories;

    /**
     * @MongoDB\String  @MongoDB\UniqueIndex
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $name;

    /**
     * @MongoDB\Float
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $price = 0;

    /**
    * @MongoDB\String
    * @JMS\Groups({"getlesson"})
    * @JMS\Expose
    */
    protected $image = "";

    /**
    * @MongoDB\String
    * @JMS\Groups({"getlesson"})
    * @JMS\Expose
    */
    protected $uptime = "";

    /**
     * @MongoDB\String
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $description = "";

    /**
     * @MongoDB\String
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $index = "";

    /**
     * @MongoDB\Int
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $status = 0;

    /**
     * @MongoDB\Int
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $learn = 0;

    /**
     * @MongoDB\Int
     * @JMS\Groups({"getlesson"})
     * @JMS\Expose
     */
    protected $order = 0;

    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param Site\LessonBundle\Document\Type $type
     * @return self
     */
    public function setType(\Site\LessonBundle\Document\Type $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return Site\LessonBundle\Document\Type $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set level
     *
     * @param Site\LessonBundle\Document\Level $level
     * @return self
     */
    public function setLevel(\Site\LessonBundle\Document\Level $level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * Get level
     *
     * @return Site\LessonBundle\Document\Level $level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set teacher
     *
     * @param Site\TeacherBundle\Document\Teacher $teacher
     * @return self
     */
    public function setTeacher(\Site\TeacherBundle\Document\Teacher $teacher)
    {
        $this->teacher = $teacher;
        return $this;
    }

    /**
     * Get teacher
     *
     * @return Site\TeacherBundle\Document\Teacher $teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * Add user
     *
     * @param Site\UserBundle\Document\User $user
     */
    public function addUser(\Site\UserBundle\Document\User $user)
    {
        $this->users[] = $user;
    }

    /**
     * Remove user
     *
     * @param Site\UserBundle\Document\User $user
     */
    public function removeUser(\Site\UserBundle\Document\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection $users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add category
     *
     * @param Site\LessonBundle\Document\Category $category
     */
    public function addCategory(\Site\LessonBundle\Document\Category $category)
    {
        $this->categories[] = $category;
    }

    /**
     * Remove category
     *
     * @param Site\LessonBundle\Document\Category $category
     */
    public function removeCategory(\Site\LessonBundle\Document\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return Doctrine\Common\Collections\Collection $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return float $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set uptime
     *
     * @param string $uptime
     * @return self
     */
    public function setUptime($uptime)
    {
        $this->uptime = $uptime;
        return $this;
    }

    /**
     * Get uptime
     *
     * @return string $uptime
     */
    public function getUptime()
    {
        return $this->uptime;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set index
     *
     * @param string $index
     * @return self
     */
    public function setIndex($index)
    {
        $this->index = $index;
        return $this;
    }

    /**
     * Get index
     *
     * @return string $index
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * Set status
     *
     * @param int $status
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set learn
     *
     * @param int $learn
     * @return self
     */
    public function setLearn($learn)
    {
        $this->learn = $learn;
        return $this;
    }

    /**
     * Get learn
     *
     * @return int $learn
     */
    public function getLearn()
    {
        return $this->learn;
    }
}
