<?php

namespace Site\CoreBundle\Library;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Site\CoreBundle\InterFacer\Library;
class File extends Library
{
	private $engine = null;
	public function __construct($container)
	{
		parent::__construct($container);
		$oss_engine = $this->container->getParameter('oss_engine');
		if($oss_engine != null)
			$this->setEngine($oss_engine);
	}
	public function setEngine($engine)
	{
		$this->engine = $engine;
	}
	public function getEngine()
	{
		if($this->engine != null)
		{
			return $this->getLibrary()->get('SiteCoreBundle:File/Oss')->get($oss_engine);
		}else{
			return $this->getLibrary()->get('SiteCoreBundle:File/Local');
		}
	}
	public function __call( $method , $args )  
    {  
        return call_user_func_array( array( $this->getEngine() , $method ) , $args );  
    }  
}