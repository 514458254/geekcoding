<?php
namespace Site\CoreBundle\Model;
use Site\CoreBundle\InterFacer\Model as BaseModel;
class System extends BaseModel
{
    public function getShow()
    {
    	return $this->rp->findOneBy(array('show' => true));
    }
    public function getList()
    {
    	return $this->rp->findAll();
    }
}
