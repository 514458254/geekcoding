define(['../config'], function (config) {
    config.controller('lessonCtrl', ['$scope', 'lessoninfo',"loaderService",
     function ($scope, lessoninfo,loaderService) {
        $scope.lesson = lessoninfo;
        loaderService.page('.user-content','.user-main','bounceDown');
    }]);
});