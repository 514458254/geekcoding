define([
    '../config',
], function (config) {
    var routing = {
        menu: Routing.generate('site_teacher_template_menu'),
        layoutcontent: Routing.generate('site_teacher_template_layoutcontent')
    };
    config.config(function ($stateProvider) {
        $stateProvider
            .state('teacher_home', {
                abstruct: true,
                views: {
                    "@": {
                        templateUrl: routing.layoutcontent,
                    },
                    "teacher-menu@teacher_home": {
                        templateUrl: routing.menu
                    }
                }
            });
    });
    return config;
});