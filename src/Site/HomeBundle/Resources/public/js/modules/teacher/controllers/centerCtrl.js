define(['../config'], function (config) {
    config.controller('centerCtrl',["$scope","centerinfo","loaderService",
        function ($scope,centerinfo) {
        $scope.phones = [
            {"name": "Nexus S",
                "snippet": "Fast just got faster with Nexus S."},
            {"name": "Motorola XOOM™ with Wi-Fi",
                "snippet": "The Next, Next Generation tablet."},
            {"name": "MOTOROLA XOOM™",
                "snippet": "The Next, Next Generation tablet."}
        ];
        loaderService.page('.teacher-content','.teacher-main','bounceDown');
    }]);
});