define(['../config'], function (config) {
    config.factory('loaderService', ['$route', '$q', '$injector', function ($route, $q, $injector) {
        return {
            getData: function (servicename) {
                var delay = $q.defer();
                Service = $injector.get(servicename);
                Service.query(function (successData) {
                    delay.resolve(successData);
                }, function (errorData) {
                    delay.reject('Unable to query this');
                });
                return delay.promise;
            },
            delay: function ($q, $defer) {
                var delay = $q.defer();
                $defer(delay.resolve, 500);
                return delay.promise;
            }
        }
    }]);
});