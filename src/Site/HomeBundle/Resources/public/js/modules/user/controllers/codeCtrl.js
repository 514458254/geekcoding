define(['../config'], function (config) {
    config.controller('codeCtrl', ['$scope', 'codeinfo',"loaderService",
     function ($scope, codeinfo,loaderService) {
        $scope.code = codeinfo;
        loaderService.page('.user-content','.user-main','bounceDown');
    }]);
});