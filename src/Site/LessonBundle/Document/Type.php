<?php 
namespace Site\LessonBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="lesson_types",repositoryClass="Site\LessonBundle\Repository\TypeRepository")
 */
class Type
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** @MongoDB\ReferenceOne(targetDocument="Lesson", mappedBy="type") */
    private $lessons;

    /**
     * @MongoDB\String @MongoDB\UniqueIndex
     */
    protected $name;

    /**
    * @MongoDB\Int
    */
    protected $order;

    /**
    * @MongoDB\String
    */
    protected $path;

    /**
    * @MongoDB\String
    */
    protected $index;
    public function __construct()
    {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lessons
     *
     * @param Site\LessonBundle\Document\Lesson $lessons
     * @return self
     */
    public function setLessons(\Site\LessonBundle\Document\Lesson $lessons)
    {
        $this->lessons = $lessons;
        return $this;
    }

    /**
     * Get lessons
     *
     * @return Site\LessonBundle\Document\Lesson $lessons
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set order
     *
     * @param int $order
     * @return self
     */
    public function setOrder($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Get order
     *
     * @return int $order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return self
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get path
     *
     * @return string $path
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set index
     *
     * @param string $index
     * @return self
     */
    public function setIndex($index)
    {
        $this->index = $index;
        return $this;
    }

    /**
     * Get index
     *
     * @return string $index
     */
    public function getIndex()
    {
        return $this->index;
    }
}
