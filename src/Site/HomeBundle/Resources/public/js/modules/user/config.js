define(['angular','../loader','../global/app', 'angularresource','angularui'], function (angular,loader) {
    var config = angular.module('user', ['ngResource','ui.router','global'])
        .config(function ($routeProvider, $interpolateProvider, $locationProvider) {
            $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
            // $locationProvider.html5Mode(true);
        }).run(["$rootScope", "$location","$injector","$state",function($rootScope, $location,$injector,$state){
        	loaderService = $injector.get("loaderService");
        	$rootScope.init = function(){
                loader.pageloader.show(".user-loader");
        	};
        	$rootScope.$on('$viewContentLoaded', function(){
		        loader.pageloader.hide();
		    });
        }]);
        return config;
});
