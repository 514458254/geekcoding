define(['../config'], function (config) {
    config.controller('planCtrl', ['$scope', 'planinfo',"loaderService",
     function ($scope, planinfo,loaderService) {
        $scope.plan = planinfo;
        loaderService.page('.user-content','.user-main','bounceDown');
    }]);
});