define(['../config'], function (config) {
    config.controller('planCtrl', ['$scope', 'planinfo',"loaderService",
     function ($scope, planinfo,loaderService) {
        $scope.plan = planinfo;
        loaderService.page('.teacher-content','.teacher-main','bounceDown');
    }]);
});