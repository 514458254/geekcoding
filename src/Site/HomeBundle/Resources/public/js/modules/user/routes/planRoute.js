define([
    './globalRoute',
    '../controllers/planCtrl',
    '../services/planService'
], function (config) {
    config.config(function ($stateProvider) {
        $stateProvider
            .state('user_home.plan', {
                url: "^"+Routing.generate('site_user_template_plan'),
                views: {
                    "user-content": {
                        templateUrl: Routing.generate('site_user_template_plan'),
                        controller: 'planCtrl',
                        resolve: {
                            planinfo: function (viewService) {
                                return viewService.getData('planService');
                            }
                        }
                    }
                }
            });
    });
});