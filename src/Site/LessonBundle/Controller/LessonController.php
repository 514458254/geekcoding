<?php

namespace Site\LessonBundle\Controller;

use Site\LessonBundle\Controller\BaseController as BaseController;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
class LessonController extends BaseController
{
	public function indexAction()
	{
		// if($this->UserLib->checkLogin() == false){
		// 	$this->setReferer($this->getRequest()->getUri());
		// 	if($this->getReferer()){
        // 		return $this->redirect($this->generateUrl('fos_user_security_login'));
		// 	}
		// }
		// $cate = $this->LessonModel->getOneShow($index)->getCategories();
		// $data = array('lesson' => $this->LessonModel->getOneShow($index));
  //       $serializer = SerializerBuilder::create()->build();
  //       $context = SerializationContext::create();
  //       // $context->setSerializeNull(true);
  //       $context->setGroups(array('getlesson'));
  //       $context->enableMaxDepthChecks();
  //       $data = $serializer->serialize($data, 'json',$context);
  //       echo $data;
		return $this->render('SiteLessonBundle:Public:layout.html.twig');
	}
	public function getAction()
	{
		return $this->render('SiteLessonBundle:Lesson:lessonget.html.twig');
	}
	public function infoAction()
	{
		return $this->render('SiteLessonBundle:Lesson:lessoninfo.html.twig');
	}
	public function chapterListAction()
	{
		return $this->render('SiteLessonBundle:Lesson:chapterlist.html.twig');
	}
}