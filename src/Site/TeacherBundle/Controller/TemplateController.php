<?php

namespace Site\TeacherBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Site\CoreBundle\InterFacer\AnnotationController as BaseController;

/** 
* @Route("/teacher/home",service="site_teacher.template")
*/
class TemplateController extends BaseController
{
    public $view_path = 'SiteTeacherBundle:Template';
    /**
     * @Route("/layoutcontent",options={"expose"=true})
     */
    public function layoutContentAction()
    {
        return $this->render("SiteTeacherBundle:Template:layout_content.html.twig");
    }
    /**
     * @Route("/menu",options={"expose"=true})
     */
    public function menuAction()
    {
        return $this->render("SiteTeacherBundle:Template:public/menu.html.twig");
    }
	/**
     * @Route("/",options={"expose"=true})
     */
    public function centerAction()
    {
        return $this->compateRender('layout.html.twig','center/index.html.twig');
    }
    /**
     * @Route("/plan",options={"expose"=true})
     */
    public function planAction()
    {
        return $this->compateRender('layout.html.twig','plan/index.html.twig');
    }
    /**
     * @Route("/lesson",options={"expose"=true})
     */
    public function lessonAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $data['lessons'] = $user->getLessons();
        return $this->compateRender('layout.html.twig','lesson/index.html.twig',$data);
    }
    /**
     * @Route("/code",options={"expose"=true})
     */
    public function codeAction()
    {
        return $this->compateRender('layout.html.twig','code/index.html.twig');
    }
}