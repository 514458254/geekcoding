<?php 
namespace Site\LessonBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Site\LessonBundle\Document\Chapter;

class LoadChapterData extends AbstractFixture implements OrderedFixtureInterface
{
	/**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
    	$chapters = array(
    		array(
                'name' => 'Symfony2的安装与快速上手',
                'class' => 1,
                'index' => 'symfony2first',
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'Symfony2的架构详解-控制器',
                'class' => 2,
                'index' => 'symfony2second',
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'Symfony2的架构详解-Twig视图',
                'class' => 3,
                'index' => 'symfony2third',
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'Symfony2的架构详解-Doctrine ORM的使用',
                'class' => 4,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'DoctrineMongodbBundle与ODM结合快速构建Mongodb程序',
                'class' => 5,
                'lesson' => 'symfony2',
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'Container容器与依赖注入',
                'class' => 6,
                'lesson' => 'symfony2',
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'DoctrineFixturesBundle写入初始数据',
                'class' => 7,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'SensioFrameworkExtraBundle的使用以及Annotation Route',
                'class' => 8,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => '完成适合自己的架构-重写控制器及类库系统搭建',
                'class' => 9,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => '使用FOSUserBundle快速构建用户系统初步',
                'class' => 10,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'NelmioApiDocBundle快速写注释及phpdocument生成',
                'class' => 11,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => 'Symfony2单元测试的编写',
                'class' => 12,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => '分析Symfony2核心源代码及利用容器随意修改',
                'class' => 13,
                'category' => 'basesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—完成基本架构及类库的构建',
                'class' => 14,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—单元测试及基础控制器的编写',
                'class' => 15,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—Bootstrap3+Less构建基本UI以及GregwarImageBundle处理图片',
                'class' => 16,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—HearsayRequireJSBundle使用Requirejs加载JS脚本',
                'class' => 17,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—DoctrineMongodbBundle完成Mongodb数据的数据构建',
                'class' => 18,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—FOSUserBundle完成用户系统',
                'class' => 19,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—SonataAdminBundle整合FOSUserBundle完成系统后台',
                'class' => 20,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—HWIOAuthBundle整合FOSUserBundle完成第三方登录',
                'class' => 21,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—FOSRestfulBunlde结合Emberjs的Restful API及客户端搭建',
                'class' => 22,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—完成所有路由及控制器的编写',
                'class' => 23,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '在线教育网站构建—给网站进行前端方面的改进',
                'class' => 24,
                'category' => 'advancesymfony2'
            ),
            array(
                'name' => '服务器的环境的搭建及上线测试',
                'class' => 25,
                'category' => 'advancesymfony2'
            ),
    	);
    	$this->insertData($manager,$chapters);
    }
    protected function insertData($manager,$chapters)
    {
    	foreach ($chapters as $key => $value) {
            $chapter = new Chapter();
            $chapter->setName($value['name']);
            $chapter->setClass($value['class']);
            $chapter->setCategory($this->getReference($value['category'].'category'));
    		$manager->persist($chapter);
    		$manager->flush();
            if(isset($value['index'])){
                $this->addReference($value['index'].'chapter', $chapter);
            }
    	}
    }
    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9;
    }
}