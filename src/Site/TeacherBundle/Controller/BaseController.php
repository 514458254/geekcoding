<?php
namespace Site\TeacherBundle\Controller;

use Site\CoreBundle\InterFacer\AnnotationController;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BaseController extends AnnotationController
{
    public function initialize(){}
}