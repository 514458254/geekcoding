<?php 
namespace Site\TeacherBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="teachers",repositoryClass="Site\TeacherBundle\Repository\TeacherRepository")
 */
class Teacher
{
    /**
     * @MongoDB\Id(strategy="INCREMENT")
     */
    protected $id;

    /** @MongoDB\ReferenceOne(targetDocument="Site\UserBundle\Document\User",inversedBy="teacher") */
    private $user;

    /** @MongoDB\ReferenceMany(targetDocument="Site\LessonBundle\Document\Lesson",mappedBy="teacher") */
    private $lessons;

    /** @MongoDB\ReferenceMany(targetDocument="Site\LessonBundle\Document\Testtype",mappedBy="teacher") */
    private $testtypes;

    /**
    * @MongoDB\Date
    */
    protected $ctime;

    public function __construct()
    {
        $this->lessons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->testtypes = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int_id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param Site\UserBundle\Document\User $user
     * @return self
     */
    public function setUser(\Site\UserBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Site\UserBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add lesson
     *
     * @param Site\LessonBundle\Document\Lesson $lesson
     */
    public function addLesson(\Site\LessonBundle\Document\Lesson $lesson)
    {
        $this->lessons[] = $lesson;
    }

    /**
     * Remove lesson
     *
     * @param Site\LessonBundle\Document\Lesson $lesson
     */
    public function removeLesson(\Site\LessonBundle\Document\Lesson $lesson)
    {
        $this->lessons->removeElement($lesson);
    }

    /**
     * Get lessons
     *
     * @return Doctrine\Common\Collections\Collection $lessons
     */
    public function getLessons()
    {
        return $this->lessons;
    }

    /**
     * Set ctime
     *
     * @param date $ctime
     * @return self
     */
    public function setCtime($ctime)
    {
        $this->ctime = $ctime;
        return $this;
    }

    /**
     * Get ctime
     *
     * @return date $ctime
     */
    public function getCtime()
    {
        return $this->ctime;
    }
}
