<?php

namespace Site\LessonBundle\Controller;

use Site\LessonBundle\Controller\BaseController as BaseController;
use Symfony\Component\HttpFoundation\Request;
class ChapterController extends BaseController
{
	public function getAction()
	{
		return $this->render('SiteLessonBundle:Chapter:chapterget.html.twig');
	}
}